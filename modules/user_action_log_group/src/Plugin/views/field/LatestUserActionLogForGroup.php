<?php

/**
 * @file
 * Definition of Drupal\user_action_log_group\Plugin\views\field\RiskLevel
 */

namespace Drupal\user_action_log_group\Plugin\views\field;

use Carbon\Carbon;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler to show the risk level of the company
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("latest_user_action_log_for_group")
 */
class LatestUserActionLogForGroup extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {
    // Get the company.
    $gid = $values->_entity->id();
    $last_logged_user = FALSE;

    // Get the last logged-in user of the company.
    if ($gid) {
      $last_logged_user = \Drupal::database()->select('user_action_log', 'ual')
        ->condition('operation', ['login', 'logout'], 'NOT IN')
        ->condition('gid', $gid)
        ->orderBy('timestamp', 'DESC')
        ->range(NULL, 1)
        ->fields('ual', ['uid', 'timestamp', 'operation', 'entity_id','entity_type'])
        ->execute()
        ->fetchAssoc();
    }

    // Construct the markup.
    $markup = new FormattableMarkup('No logged action available yet', []);

    if ($last_logged_user !== FALSE && count($last_logged_user)) {
      $user = \Drupal::entityTypeManager()->getStorage('user')->load($last_logged_user['uid']);
      $name = \Drupal::service('dana.user_controller')->getFullName($user);
      $time = Carbon::createFromTimestamp($last_logged_user['timestamp'])->format('d-m-Y H:i');
      $operation_string =  $this->getOperationString($last_logged_user['operation']);
      $markup = new FormattableMarkup($name . ' has ' . $operation_string . ' at ' . $time, []);
    }

    // Return the markup.
    $render = [
      '#markup' => trim($markup),
      '#cache' =>[
        'max-age' => 0
      ]
    ];

    return $render;
  }

  /**
   * Returns the operation string markup.
   *
   * @param $operation
   * @param $entity_type
   * @param $entity_id
   *
   * @return string
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getOperationString($operation) {

    // Setup operation mapping.
    $operation_mapping = [
      'update' => 'edited',
      'insert' => 'added',
      'delete' => 'deleted'
    ];

    if (isset($operation_mapping[$operation])) {
      $operation = $operation_mapping[$operation];
    }

    // Construct the operation string.
    return $operation;
  }
}
