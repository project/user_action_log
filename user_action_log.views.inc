<?php

/**
 * @file
 * Provide views data for user_action_log.module.
 */

/**
 * Implements hook_views_data().
 */
function user_action_log_views_data() {
  $data = [];

  $data['user_action_log']['table']['group'] = t('User Action Log');
  $data['user_action_log']['table']['wizard_id'] = 'user_action_log';

  $data['user_action_log']['table']['base'] = [
    'field' => 'id',
    'title' => t('user Action Log entries'),
    'help' => t('Contains a list of user action log entries.'),
  ];

  $data['user_action_log']['id'] = [
    'title' => t('ID'),
    'help' => t('Unique user action log event ID.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['user_action_log']['uid'] = [
    'title' => t('UID'),
    'help' => t('The user ID of the user on which the user action log entry was written.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
    'relationship' => [
      'title' => t('User'),
      'help' => t('The user on which the log entry as written.'),
      'base' => 'users_field_data',
      'base field' => 'uid',
      'id' => 'standard',
    ],
  ];

  $data['user_action_log']['operation'] = [
    'title' => t('Operation'),
    'help' => t('The operation that was executed.'),
    'field' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['user_action_log']['entity_id'] = [
    'title' => t('Entity id'),
    'help' => t('The entity id for which the log was written.'),
    'field' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['user_action_log']['entity_type'] = [
    'title' => t('Entity type'),
    'help' => t('The entity type for which the log was written.'),
    'field' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['user_action_log']['bundle'] = [
    'title' => t('Bundle'),
    'help' => t('The bundle of the entity type.'),
    'field' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['user_action_log']['timestamp'] = [
    'title' => t('Created'),
    'help' => t('Date when the event occurred.'),
    'field' => [
      'id' => 'date',
    ],
    'argument' => [
      'id' => 'date',
    ],
    'filter' => [
      'id' => 'date',
    ],
    'sort' => [
      'id' => 'date',
    ],
  ];

  return $data;
}
