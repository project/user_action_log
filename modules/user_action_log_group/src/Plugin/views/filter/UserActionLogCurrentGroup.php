<?php

namespace Drupal\user_action_log_group\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;

/**
 * Filter by current group.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("user_action_log_current_group")
 */
class UserActionLogCurrentGroup extends FilterPluginBase {

  /**
   * @return string|void
   */
  public function adminSummary() {}

  /**
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  protected function operatorForm(&$form, FormStateInterface $form_state) {}

  /**
   * @return bool
   */
  public function canExpose() {
    return FALSE;
  }

  /**
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function query() {
    $table = $this->ensureMyTable();

    if (!empty(\Drupal::hasService('group_purl.context_provider'))) {
      if ($group = \Drupal::service('group_purl.context_provider')->getGroupFromRoute()) {
        $entity_ids_of_group = $this->getEntityIdsOfGroup($group->id());
        $this->query->addWhereExpression($this->options['group'], "$table.entity_id IN (" . implode(', ', $entity_ids_of_group) .  ")");
      }
    }
  }

  /**
   * Returns the entity ids of the given group id.
   *
   * @param $gid
   *
   * @return array
   */
  private function getEntityIdsOfGroup($gid) {
    $result = \Drupal::database()->select('group_content_field_data', 'gcfd')
      ->condition('gid', $gid)
      ->fields('gcfd', ['entity_id'])
      ->execute();

    $entity_ids_of_group = [];
    while ($row = $result->fetchAssoc()) {
      $entity_ids_of_group[] = $row['entity_id'];
    }

    return $entity_ids_of_group;
  }
}