<?php

/**
 * @file
 * Definition of Drupal\user_action_log\Plugin\views\field\AuditScore
 */

namespace Drupal\user_action_log\Plugin\views\field;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler for the entity label.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("user_action_log_entity_label")
 */
class UserActionLogEntityLabel extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * Define the available options
   * @return array
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    return $options;
  }

  /**
   * Provide the options form.
   *
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * @{inheritdoc}
   *
   * @param \Drupal\views\ResultRow $values
   *
   * @return string
   */
  public function render(ResultRow $values) {
    if ($values->user_action_log_entity_id && $values->user_action_log_entity_type) {
      $id = $values->user_action_log_entity_id;
      $entity_type = $values->user_action_log_entity_type;
      $entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($id);

      return $entity->label();
    }
  }

  /**
   * Check on permission.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *
   * @return bool|mixed
   */
  function access(AccountInterface $account) {
    return parent::access($account);
  }
}