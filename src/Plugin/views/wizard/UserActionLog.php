<?php

namespace Drupal\user_action_log\Plugin\views\wizard;

use Drupal\views\Plugin\views\wizard\WizardPluginBase;

/**
 * Defines a wizard for the watchdog table.
 *
 * @ViewsWizard(
 *   id = "user_action_log",
 *   module = "user_action_log",
 *   base_table = "user_action_log",
 *   title = @Translation("User Action Log entries")
 * )
 */
class UserActionLog extends WizardPluginBase {

  /**
   * Set the created column.
   *
   * @var string
   */
  protected $createdColumn = 'timestamp';

  /**
   * {@inheritdoc}
   */
  protected function defaultDisplayOptions() {
    $display_options = parent::defaultDisplayOptions();

    // Add permission-based access control.
    $display_options['access']['type'] = 'perm';
    $display_options['access']['options']['perm'] = 'access site reports';

    return $display_options;
  }

}
