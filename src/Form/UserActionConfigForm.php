<?php

namespace Drupal\user_action_log\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a configuration form for the user action log.
 */
class UserActionConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_action_log_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['user_action_log.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('user_action_log.settings');

    $form['user_action_log'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('User action log settings'),
    ];

    $entity_types = \Drupal::entityTypeManager()->getDefinitions();
    $entity_type_options = [];

    foreach($entity_types as $entity_type_key => $entity_type) {

      if ($entity_type->getProvider() !== 'group') {

        if ($entity_type->getPermissionGranularity() == 'bundle') {
          $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entity_type->id());

          foreach ($bundles as $bundle_key => $bundle_label) {
            $entity_type_options[$entity_type_key . '_' . $bundle_key] = $entity_type->getLabel() . ': ' . $bundle_label['label'];
          }
        }
      }
    }

    $default_value = $config->get('entity_types_to_log');
    $form['user_action_log']['entity_types_to_log'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Entity types / bundles to log'),
      '#options' => $entity_type_options,
      '#default_value' => is_array($default_value) ? $default_value : []
    ];

    $form['user_action_log']['log_users'] = [
      '#type' => 'radios',
      '#title' => $this->t('Log user creation / update'),
      '#options' => [1 => 'Yes', 0 => 'No'],
      '#default_value' => $config->get('log_users') ? $config->get('log_users') : 0
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('user_action_log.settings');

    $form_state->cleanValues();
    $values = $form_state->getValues();

    foreach ($values as $key => $value) {
      $config->set($key, $value);
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }
}
