<?php

namespace Drupal\user_action_log\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class UserActionController
 */
class UserActionController extends ControllerBase {

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * UserActionController constructor.
   */
  public function __construct() {
    $this->connection = \Drupal::database();
  }

  /**
   * Prepares an entity for logging
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param $operation
   */
  public function prepareEntityForActionLog($entity, $operation) {
    $bundles_to_log = \Drupal::config('user_action_log.settings')->get('entity_types_to_log');
    $uid = \Drupal::currentUser()->id();

    $entity_type = $entity->getEntityTypeId();
    $bundle = $entity->bundle();

    if (isset($bundles_to_log[$entity_type . '_' . $bundle]) && $bundles_to_log[$entity_type . '_' . $bundle]) {

      try {
        $this->logAction($uid, $operation, $entity->id(), $entity_type, $bundle);
      }
      catch (\Exception $e) {
        \Drupal::logger('user_action_log')->error($e->getMessage());
      }
    }

    $log_users = \Drupal::config('user_action_log.settings')->get('log_users');
    if ($entity_type == 'user' && $log_users) {
      try {
        $this->logAction($uid, $operation, $entity->id(), $entity_type);
      }
      catch (\Exception $e) {
        \Drupal::logger('user_action_log')->error($e->getMessage());
      }
    }
  }

  /**
   * Logs an action.
   *
   * @param $uid
   * @param $operation
   * @param null $entity_id
   * @param null $entity_type
   * @param null $bundle
   *
   * @return \Drupal\Core\Database\StatementInterface|int|null
   * @throws \Exception
   */
  public function logAction($uid, $operation, $entity_id = NULL, $entity_type = NULL, $bundle = NULL) {

    $fields = [
      'uid' => $uid,
      'operation' => $operation,
      'entity_id' => $entity_id,
      'entity_type' => $entity_type,
      'bundle' => $bundle,
      'timestamp' => time()
    ];

    \Drupal::moduleHandler()->alter('log_action_fields', $fields);

    return $this->connection->insert('user_action_log')
      ->fields($fields)
      ->execute();
  }
}