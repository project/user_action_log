<?php

/**
 * @file
 * Post update functions for the User Action Logging module.
 */

use Drupal\Core\Config\FileStorage;
use Drupal\Core\Config\InstallStorage;
use Drupal\views\Entity\View;

/**
 * Replace 'User Action Log' with a view.
 */
function user_action_log_post_update_convert_recent_messages_to_view() {
  // Only create if the views module is enabled and the watchdog view doesn't
  // exist.
  if (\Drupal::moduleHandler()->moduleExists('views')) {
    if (!View::load('user_action_log')) {
      // Save the watchdog view to config.
      $module_handler = \Drupal::moduleHandler();
      $optional_install_path = $module_handler->getModule('user_action_log')->getPath() . '/' . InstallStorage::CONFIG_OPTIONAL_DIRECTORY;
      $storage = new FileStorage($optional_install_path);

      \Drupal::entityTypeManager()
        ->getStorage('view')
        ->create($storage->read('views.view.user_action_log'))
        ->save();

      return t('The user action log view has been created.');
    }

    return t("The user action log view already exists and was not replaced. To replace the 'User Action Log' with a view, rename the user action log view and uninstall and install the 'User Action Log' module");
  }
}
